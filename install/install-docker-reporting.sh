#!/bin/bash
set -e

echo "Installing pack reporting..."
mkdir -p /etc/docker/compose
cd /etc/docker/compose
wget https://gitlab.com/xivoxc/packaging/raw/master/install/docker-reporting.yml
read -p "Enter the XiVO IP address: " XIVO_HOST
sed -i "s/XIVO_HOST=.*/XIVO_HOST=$XIVO_HOST/" docker-reporting.yml
read -p "Enter the number of weeks to keep: " WEEKS_TO_KEEP
sed -i "s/WEEKS_TO_KEEP=.*/WEEKS_TO_KEEP=$WEEKS_TO_KEEP/" docker-reporting.yml
mkdir -p /var/log/xivocc
chown -R daemon:daemon /var/log/xivocc
docker-compose -f /etc/docker/compose/docker-reporting.yml pull
echo "The pack reporting is installed. To launch it, execute 'docker-compose -f /etc/docker/compose/docker-reporting.yml up -d'. If a pack reporting was previously installed with the old packaging system, please migrate the database first."
