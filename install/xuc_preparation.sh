#!/bin/sh
# Copyright (C) 2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

XIVOCC_HOST=$1
REPLY=$2

	    # Edit postgresql configuration files
	    echo -e "\e[1;32mXiVO PBX editing postgresql.conf\e[0m"
	    sed -i.bak '/listen_addresses/s/localhost/*/' /etc/postgresql/9.4/main/postgresql.conf
	    sed -i.bak '/listen_addresses/s/#listen_addresses/listen_addresses/' /etc/postgresql/9.4/main/postgresql.conf
        echo -e "\e[1;32mXiVO PBX editing pg_hba.conf\e[0m"
        echo "host  asterisk    all $XIVOCC_HOST/32  md5" >> /etc/postgresql/9.4/main/pg_hba.conf

        # Add new user for statistics
        echo -e "\e[1;32mXiVO PBX Creating a new user for statistics in the database\e[0m"
        sudo -u postgres psql asterisk -c "CREATE USER stats WITH PASSWORD 'stats'; GRANT SELECT ON ALL TABLES IN SCHEMA PUBLIC TO stats;"

        # Asterisk configuration
        touch /etc/asterisk/manager.d/02-xivocc.conf
        echo -e "\e[1;32mXiVO PBX Creating 02-xivocc.conf\e[0m"
        echo "
[xuc]
secret = xucpass
deny=0.0.0.0/0.0.0.0
permit=$XIVOCC_HOST/255.255.255.255
read = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
write = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
" > /etc/asterisk/manager.d/02-xivocc.conf
        echo -e "\e[1;32mXiVO PBX Reloading Asterisk Manager\e[0m"
        asterisk -rx "manager reload"
        echo -e "\e[1;32mXiVO PBX Verify configuration\e[0m"
        asterisk -rx "manager show user xuc"
        echo -e "\e[1;32mXiVO PBX Editing Asterisk cel.conf\e[0m"
        sed -i.bak '/\[general\]/,/enable/ s/ =.*/ = yes/' /etc/asterisk/cel.conf
        sed -i.bak '/\[general\]/,/apps = / s/apps = .*/apps = dial,park,queue/' /etc/asterisk/cel.conf
        sed -i.bak  '/\[general\]/,/events/ s/events = .*/events = APP_START,CHAN_START,CHAN_END,ANSWER,HANGUP,BRIDGE_ENTER,BRIDGE_EXIT,USER_DEFINED,LINKEDID_END,HOLD,UNHOLD,BLINDTRANSFER,ATTENDEDTRANSFER/' /etc/asterisk/cel.conf
        sed -i.bak '/\[manager\]/,/enabled/ s/enabled = .*/enabled = yes/' /etc/asterisk/cel.conf
        echo -e "\e[1;32mXiVO PBX Reloading module cel\e[0m"
        asterisk -rx "module reload cel"

        # Add XUC specific user and websocket service to the database
        cd /tmp
        echo -e "\e[1;32mXiVO PBX Inserting values into database\e[0m"
	    echo -e "\e[1;32mXiVO PBX Creating xuc user\e[0m"
	    sudo -u asterisk psql asterisk -c "INSERT INTO func_key_template (name, private) VALUES ('xuc', 'true');"
	    sudo -u asterisk psql asterisk -c "INSERT INTO userfeatures(uuid,firstname,email,agentid,pictureid,entityid,callerid,ringseconds,simultcalls,enableclient,loginclient,passwdclient,cti_profile_id,enablehint,enablevoicemail,enablexfer,enableonlinerec,callrecord,incallfilter,enablednd,enableunc,destunc,enablerna,destrna,enablebusy,destbusy,musiconhold,outcallerid,mobilephonenumber,bsfilter,preprocess_subroutine,timezone,language,ringintern,ringextern,ringgroup,ringforward,rightcallcode,commented,func_key_private_template_id,lastname,userfield,description) VALUES('d5dd4e90-c6ce-4b0e-a85f-419ae3cc3561','xuc','',0,0,1,'xuc',30,5,1,'xuc','0000',1,1,0,1,0,0,0,0,0,'',0,'',0,'','default','default','','no','','','','','','','','',0,(SELECT id FROM func_key_template WHERE name='xuc' LIMIT 1),'','','');"
	    echo -e "\e[1;32mXiVO PBX Creating web service\e[0m"
        sudo -u asterisk psql asterisk -c "INSERT INTO accesswebservice(name,login,passwd,host,description) VALUES('xivows','xivows','','${XIVOCC_HOST}','XiVO-CC WS')"
        echo -e "\e[1;32mXiVO PBX Adding repository\e[0m"

        # Restart XiVO PBX if answer was yes
        echo -e "\e[1;32mXiVO PBX restarting services\e[0m"
        if [ "$REPLY" = true ]; then
            echo -e "\e[1;32mXiVO PBX is going to restart NOW\e[0m"
            sleep 3s
            xivo-service restart all
        else
            echo -e "\e[1;32mPlease restart XiVO PBX later\e[0m"
        fi
	    echo -e "\e[1;32mXiVO PBX Done. Thank you.\e[0m"
