#!/bin/bash
set -e

echo "Installing XiVO CC..."
# Generic XiVO CC config
mkdir -p /etc/docker/compose
cd /etc/docker/compose
wget https://gitlab.com/xivoxc/packaging/raw/master/install/docker-xivocc.yml
read -p "Enter the XiVO IP address: " XIVO_HOST
sed -i "s/XIVO_HOST=.*/XIVO_HOST=$XIVO_HOST/" docker-xivocc.yml
sed -i "s/\"xivo_host:.*/\"xivo_host:$XIVO_HOST\"/" docker-xivocc.yml
mkdir -p /var/log/xivocc
chown -R daemon:daemon /var/log/xivocc

# Pack reporting config
read -p "Enter the number of weeks to keep for the statistics: " WEEKS_TO_KEEP
sed -i "s/WEEKS_TO_KEEP=.*/WEEKS_TO_KEEP=$WEEKS_TO_KEEP/" docker-xivocc.yml

# Recording config
mkdir -p /var/spool/recording-server
chown daemon:daemon /var/spool/recording-server
chmod 760 /var/spool/recording-server
read -p "Enter the number of weeks to keep for the recording files: " RECORDING_WEEKS_TO_KEEP
sed -i "s/RECORDING_WEEKS_TO_KEEP=.*/RECORDING_WEEKS_TO_KEEP=$RECORDING_WEEKS_TO_KEEP/" docker-xivocc.yml

# Xuc config
read -p "Enter the external IP of the machine: " XUC_HOST
sed -i "s/XUC_HOST=.*/XUC_HOST=$XUC_HOST/" docker-xivocc.yml

# self-signed certificate for NGINX
SSLDIR=/etc/docker/nginx/ssl
mkdir -p $SSLDIR
apt-get update
apt-get install -y openssl
openssl req -nodes -newkey rsa:2048 -keyout $SSLDIR/xivoxc.key -out $SSLDIR/xivoxc.csr -subj "/C=FR/ST=/L=/O=Avencall/OU=/CN=$(hostname --fqdn)"
openssl x509 -req -days 3650 -in $SSLDIR/xivoxc.csr -signkey $SSLDIR/xivoxc.key -out $SSLDIR/xivoxc.crt

docker-compose -f /etc/docker/compose/docker-xivocc.yml pull
echo "XiVO CC is installed. To launch it, execute 'dcomp up -d'.
If a pack reporting was previously installed with the old packaging system, please migrate the database first.
If you are using XiVO CC with a version of XiVO less than 15.15, please modify /etc/docker/compose/docker-xivocc.yml to use the proper ast11 images."
