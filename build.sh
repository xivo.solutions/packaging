#!/bin/bash

usage() {
    cat <<-EOF
	usage : $0 service dockertag

	The 'service' parameter is mandatory and must be a directory in the current directory with a subdirectory docker.
	A Dockerfile and a version.txt file must be present in the docker subdirectory.

  The 'dockertag' parameter should be latest or latestrc or latestdev
	EOF

    cd $PWD
    exit 0
}

check_service() {
    if [ ! -d $SERVICE_DIR ]; then
        echo "Service directory not found"
		usage
    fi
}

check_version() {
	echo $VERSION_FILE
    if [ ! -s $VERSION_FILE ]; then
        echo "Version file not found or empty"
		usage
    fi
}

dockerize() {
	cd $SERVICE_DIR
    docker build -t xivoxc/$SERVICE:$VERSION .
    docker push xivoxc/$SERVICE:$VERSION
    docker tag xivoxc/$SERVICE:$VERSION xivoxc/$SERVICE:$RELEASE.$DOCKER_TAG
    docker push xivoxc/$SERVICE:$RELEASE.$DOCKER_TAG
	cd $PWD
}


if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters"
    usage
fi

SERVICE=$1
RELEASE=$2
DOCKER_TAG=$3
PWD=$(pwd)
SERVICE_DIR=$PWD/$SERVICE/docker
VERSION_FILE=$SERVICE_DIR/version.txt

check_service

check_version

VERSION=$(cat $VERSION_FILE)

echo "Release $RELEASE Dockerizing $SERVICE tag $DOCKER_TAG component version $VERSION"

dockerize

echo "Done"
