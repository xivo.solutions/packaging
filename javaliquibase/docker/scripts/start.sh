#!/bin/bash

updatedb()  {
    if [ -z "$REPORTING_DB_HOST" ]; then
        REPORTING_DB_HOST="db"
    fi
    echo "[INFO] - updating database on host \"$REPORTING_DB_HOST\""
    cd /opt/liquibase/sqlscripts/
    liquibase --driver="org.postgresql.Driver" \
        --changeLogFile=db.changelog.xml \
        --url="jdbc:postgresql://$REPORTING_DB_HOST:$DB_PORT/$DB_NAME" \
        --username=$DB_USERNAME --password=$DB_PASSWORD  \
        --classpath=$LIQUIBASE_HOME/postgresql-$POSTGRES_DRIVER_VERSION.jar \
        --logLevel=info \
        migrate
    return $?
}

i=1
max=5
sec=10

updatedb
while [ $? -ne 0 ] && [ $i -lt $max ]
do
    echo "[ERROR] - ["$i"] unable to update database $DB_NAME with user $DB_USERNAME waiting $sec seconds"
    i=$(( $i + 1 ))
    sleep $sec
    updatedb
done

if [ $i -eq $max ]
then
    echo "[ERROR] - unable to update database tried $i time"
    exit -1
fi

echo "[INFO] - Starting $@"
exec $@
