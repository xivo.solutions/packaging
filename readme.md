XiVOcc packaging project

* Docker yml file
* Javaliquibase docker image base for many components

Following projects historically in this repo has been moved to separate ones:

* Kibana
* Postgres with database creation scripts
* Recording synchro (copy file)
* spagobi
* nginx


Update release in release.txt file

Build and dockerise using build.sh

Parameters :
* Component
* Docker tag (latest, latestdev, latestrc)


*Copyright Avencall 2020 : www.xivo.solutions*
